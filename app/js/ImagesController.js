var imageDeta = null;
var storeDeta = null;
//Images CONTROLLER
function ImagesController($scope){

    $scope.galleries =[];
    $scope.title   = "";
    $scope.name       = "";
    $scope.status = 0;
    $scope.created   = "";
    $scope.modified   = "";
    $scope.description   = "";
    $scope.path   = "";
    $scope.storeName   = "";
    $scope.selected = null;

    var options={
        type: "galleries",
        qs:{limit:10}
    };
    $scope.init = function(){
      $('#myModal').show(); // Show modal .............
        $scope.galleries = [];
        var data;
        client.createCollection(options, function(err, data){
          console.log(data);
            if(err){
                showMessCom('#messLisImg', '<div class="alert alert-error">'+ data.error_description +'</div>');
                $('#myModal').hide();
            }else{
                $scope.drawPages2 = function(){
                    data.fetch(function(err, d){
                        $('#prevPage').hide();
                        $('#nextPage').hide();

                        //reset the pointer
                        data.resetEntityPointer();
                        $scope.galleries= data._list;
                        $scope.$apply($scope.galleries);

                        if(data.hasNextPage())
                          $('#nextPage').show();
                        if(data.hasPreviousPage())
                            $('#prevPage').show();

                        $('#myModal').hide();
                        $scope.checkAll(false);
                    });
                } //End drawUser function

                $scope.prevPage = function(){
                    data.getPreviousPage(function(err, data){
                        if(err)
                            alert(data.error_description);
                        else
                            $scope.drawPages2();
                    });
                }
                $scope.nextPage = function(){
                    data.getNextPage(function(err, data){
                        if(err)
                             alert(data.error_description);
                        else
                            $scope.drawPages2();
                    });
                };
                $scope.drawPages2();
            };//End else
        });
    };

    $scope.search = '';
    $scope.filterByTitle = function(data){
      return data._data.title.toLowerCase().search($scope.search.toLowerCase()) != -1;
    }

    $scope.check = true;
    $scope.checkAll = function(data){
      if(data)
      $('input.chk').prop('checked', true);
      else
      $('input.chk').prop('checked', false);
    }
    $scope.check1 = function(){
      var d=false;
      $("input.chk:not(:checked)").each(function() {
                  d=true;
        });
        if(d){
           $('input.checkAll').prop('checked', false);
           $scope.check = true;
        }
        else{
          $('input.checkAll').prop('checked', true);
          $scope.check = false;
        }
    }

    //"Create new user" pop-up appears
    $scope.open = function(){
      $scope.shouldBeOpen = true;
       $scope.title ="";
       $scope.path ="";
       $scope.description ="";
       $scope.status =0;
       $scope.selected = null;
        getStore();
    };

    function getStore(){
        var options={
            method: "GET",
            endpoint: "stores/",
            qs: { ql:"select uuid, name, title" }
        }

        client.request(options, function(err, data){
            $scope.storeList = [];           
            if(err)
                showMessCom('#messLisImg', '<div class="alert alert-error"> '+ data.error_description +'</div>');                        
            else{
                for (var i = 0; i < data.list.length; i++) {
                    var obj ={};
                    obj.uuid = data.list[i][0];
                    obj.name = data.list[i][1];
                    obj.title = data.list[i][2];
                    $scope.storeList.push(obj);
                };
                $scope.$apply($scope.storeList);
            }
        });
    }

    //"Create new user" pop-up disappears
    $scope.close = function(){
      $scope.shouldBeOpen = false;
    }

  $scope.cancel = function(){
      location.href = "#/admin";
  };

  $scope.resetGallery = function(){
    $scope.title= "";
    $scope.path ="";
    $scope.description ="";
    $scope.status = 0;
    $scope.storeName = "";
    $scope.selected = null;
  }

    //Delete pages function
  $scope.deleteGallery = function(){
    //$('#myModal').show();
     var array = [];
      $(".chk:checked").each(function(){
          array.push( $(this).val());
      });
       if(array.length > 0){
            if (confirm('Are you sure you want to delete?')){             
                for(var i =0; i< array.length; i++){
                    var endpoint='galleries/' + array[i] ;
                    var options={
                      method:'DELETE',
                      endpoint:endpoint
                    };
                    client.request(options, function(err, data){
                      if(err)
                        showMessCom('#messListImg', '<div class="alert alert-error">'+data.error_description +'.</div>');                        
                      else{
                        location.reload();
                      }
                    });
                }
            }
        }else{
           showMessCom('#messListImg', '<div class="alert alert-error">Please select at least a row to delete.</div>');
        }    
   }

  //Create new user function
  $scope.createGallery = function(){
    var endpoint ="";
    if($scope.title.trim() != ""){
        $('#myModal').show();
        if($scope.selected != null){
            endpoint = "stores/" + $scope.selected.uuid + "/has/galleries";
            $scope.storeName = $scope.selected.title;
        }else{
            endpoint = "galleries";
            $scope.storeName ="";
        }
        
        var options ={
          method : "POST",
          endpoint: endpoint,
          body: {
            title : $scope.title,
            name : $scope.title,
            path : $scope.path,
            description : $scope.description,
            status : $scope.status,
            storeName : $scope.storeName
          }
        };

        client.request(options, function(err, data){
            $('#myModal').hide();
            if(err){                
                showMessCom('#messCreaImg', '<div class="alert alert-error">'+ data.error_description +'</div>');
            }
            else{              
                $scope.shouldBeOpen = false;
                location.reload();
            }      
        });
    }else{
        showMessCom('#messCreaImg', '<div class="alert alert-error">Input data invalid.</div>');
    }
    
  }

  $scope.changed = function(){
    console.log($scope.selected.uuid);
  }


  $scope.galleryDetail = function(obj){
    imageDeta = obj;
  }

    $scope.predicate = '_data.title';
    $('.sort').click(function(){
        if($scope.reverse){
            $('#sortImg').remove();
            $(this).append('<span id="sortImg"> &#9650;</span>');}
        else{
            $('#sortImg').remove();
            $(this).append('<span id="sortImg"> &#9660;</span>');}
    });



}

//Gallery DETAIL CONTROLLER
function ImageDetailController($scope, $routeParams){
    $scope.uuid = $routeParams.galleryid;
    $scope.gallery={};
    $scope.stores={};
    var old_categoryuuid = "";
    var old_category = "";
    $scope.selected = {};

    $scope.init = function(){
      $('#myModal').show(); // Show modal .............
      if(imageDeta != null){
        $scope.gallery = JSON.parse(JSON.stringify(imageDeta));
        $scope.getAllStores();
      }else{
        var options={
          method: "GET",
          endpoint: "galleries/" + $scope.uuid
        }

        client.request(options, function(err, data){

            if(err)
            {
                $('#myModal').hide();
                showMessCom('#messDetailImg', '<div class="alert alert-error">'+ data.error_description +'</div>');
            }                
            else{
                $scope.gallery=data.entities[0];
                old_category = data.entities[0].storeName;
                $scope.$apply($scope.gallery);
                $scope.getAllStores();
            }
            // $('#myModal').hide(); 
        });
      }   
    }
    
    $scope.getAllStores = function(){
        var options={
          method: "GET",
          endpoint: "stores/"
        }

        client.request(options, function(err, data){
            if(err)
                showMessCom('#messDetailImg', '<div class="alert alert-error"> '+ data.error_description +'</div>');
            else{
                $scope.stores=data.entities;
                $scope.selected = data.entities[0];
                $.each(data.entities, function(key, value) {
                    console.log(value.name, $scope.gallery.storeName);
                    if(value.name === $scope.gallery.storeName){
                        $scope.selected = data.entities[key];
                        old_categoryuuid = data.entities[key].uuid;
                    }
                });
                $scope.$apply($scope.selected);
                $scope.$apply($scope.stores);
            }
             $('#myModal').hide(); 
        });         
    }

    //Navigate to Users menu
    $scope.cancel = function(){
        location.href = "#/admin";
    };
    
    $scope.update=function(uuid){
        $('#myModal').show();
        console.log($scope.selected.name, $scope.gallery.storeName);
        // delete old connection
        if($scope.selected.name != $scope.gallery.storeName){
          if(old_category != $scope.selected.name){ // if change category
            var endpoint='stores/' + old_categoryuuid + '/has/galleries/' + uuid;  
            var options={
                method:'DELETE',
                endpoint:endpoint
            };
              client.request(options, function(err, data){
                  if(err)
                      showMessCom('#messDetailImg', '<div class="alert alert-error"> '+ data.error_description +'</div>');
                  else{
                      
                  }
              });
          }
        }
        

         var optionsA = {
                type:"galleries/" + $scope.gallery.uuid
            };

        client.createEntity(optionsA, function(error, obj){
            if(error)
              alert("Error!!!");
            else{
              obj.set("title", $scope.gallery.title);
              obj.set("status",$scope.gallery.status);
              obj.set("description",$scope.gallery.description);
              obj.set("path",$scope.gallery.path);
              if($scope.selected != null){
                 obj.set("storeName",$scope.selected.name);
             }
              obj.save();
              var options = {
                method:'PUT'
              };
              client.request(options, function (err, data) {
                  if (err && client.logging) {
                    showMessCom('#messDetailImg', '<div class="alert alert-error"> '+ data.error_description +'</div>');
                  }else{
                        if($scope.selected.name != $scope.gallery.storeName){
                            var endpoint = "stores" + '/' + $scope.selected.uuid + '/' + "has" + '/' + obj.get("type") + '/' + $scope.gallery.uuid;
                            var options={
                                method:'POST',
                                endpoint:endpoint
                            };
                            client.request(options, function(err, data){
                                $('#myModal').hide();
                                if(err)
                                    showMessCom('#messDetailImg', '<div class="alert alert-error"> '+ data.error_description +'</div>');
                                else{
                                    location.href = "#/admin";
                                }
                            });
                      }else{
                        location.href = "#/admin";
                      }
                  }
                   $('#myModal').hide();
             });
            }
        });
        
    }

    $scope.resetGallery = function(){
      $scope.init();
    }

    $(document).ready(function() {
      $scope.init();
    });

}

// =============== Store CONTROLLER ========================== //
function StoresController($scope){

    $scope.stores =[];
    $scope.title   = "";
    $scope.name       = "";
    $scope.created   = "";
    $scope.modified   = "";
    $scope.locationname   = "";
    $scope.latitude   = 0;
    $scope.longtitude   = 0;

    var options={
        type: "stores",
        qs:{limit:10}
    };
    $scope.init = function(){
       $('#myModal').show(); 
        $scope.stores = [];
        var data;
        client.createCollection(options, function(err, data){
            if(err){
               showMessCom('#messStoreList', '<div class="alert alert-error">'+ data.error_description +'</div>');
                $('#myModal').hide();
            }else{
                $scope.drawPages2 = function(){
                    data.fetch(function(err, d){
                        $('#prevPage').hide();
                        $('#nextPage').hide();

                        //reset the pointer
                        data.resetEntityPointer();
                        $scope.stores= data._list;
                        $scope.$apply($scope.stores);

                        if(data.hasNextPage())
                          $('#nextPage').show();
                        if(data.hasPreviousPage())
                            $('#prevPage').show();

                        $('#myModal').hide();
                        $scope.checkAll(false);
                    });
                } //End drawUser function

                $scope.prevPage = function(){
                    $('#myModal').show();
                    data.getPreviousPage(function(err, data){
                        if(err)
                            alert("err");
                        else
                            $scope.drawPages2();
                    });
                }
                $scope.nextPage = function(){
                     $('#myModal').show();
                    data.getNextPage(function(err, data){
                        if(err)
                            alert("err");
                        else
                            $scope.drawPages2();
                    });
                };
                $scope.drawPages2();
            };//End else
        });
    };

    $scope.search = '';
    $scope.filterByTitle = function(data){
      return data._data.title.toLowerCase().search($scope.search.toLowerCase()) != -1;
    }

    $scope.check = true;
    $scope.checkAll = function(data){
      if(data)
      $('input.chk').prop('checked', true);
      else
      $('input.chk').prop('checked', false);
    }
    $scope.check1 = function(){
      var d=false;
      $("input.chk:not(:checked)").each(function() {
                  d=true;
        });
        if(d){
           $('input.checkAll').prop('checked', false);
           $scope.check = true;
        }
        else{
          $('input.checkAll').prop('checked', true);
          $scope.check = false;
        }
    }
                            

    //"Create new user" pop-up appears
    $scope.open = function(){
      $scope.shouldBeOpen = true;
    };

    //"Create new user" pop-up disappears
    $scope.close = function(){
      $scope.shouldBeOpen = false;
    }

  $scope.cancel = function(){
      location.href = "#/admin";
  };

    //Delete pages function
  $scope.delete = function(){
     
     var array = [];
      $(".chk:checked").each(function(){
          array.push( $(this).val());
      });
       if(array.length > 0){
        $('#myModal').show();
            if (confirm('Are you sure to delete?')){             
                for(var i =0; i< array.length; i++){
                    var endpoint='stores/' + array[i] ;
                    var options={
                      method:'DELETE',
                      endpoint:endpoint
                    };
                    client.request(options, function(err, data){
                         $('#myModal').hide();
                      if(err)
                        showMessCom('#messStoreList', '<div class="alert alert-error">'+data.error_description +'</div>');
                      else{
                        location.reload();
                      }
                    });
                }
            }
        }else{
           showMessCom('#messStoreList', '<div class="alert alert-error">Please select at least a row to delete.</div>');
        }    
   }

  //Create new user function
  $scope.create = function(){
     $('#myModal').show();
    var options ={
      method : "POST",
      endpoint: "stores",
      body: {
        title : $scope.title,
        name : $scope.title,
        latitude : $scope.latitude,
        locationname : $scope.locationname,
        longtitude : $scope.longtitude,
        description: $scope.description
      }
    };

    client.request(options, function(err, data){
         $('#myModal').hide();
      if(err)
        showMessCom('#messStoreAdd','<div class="alert alert-error"> '+ data.error_description +'</div>');        
      else{
        $scope.shouldBeOpen = false;
        location.reload();
      }
    });
  }

  $scope.storeDetail = function(obj){
    storeDeta = obj;
  }

  $scope.resetStore = function(){
      $scope.title = "";
      $scope.name = "";
      $scope.description = "";
      $scope.locationname   = "";
      $scope.latitude   = 0;
      $scope.longtitude   = 0;
      hideMessbox('#messStoreAdd');
    }

    $scope.predicate = '_data.title';
    $('.sort').click(function(){
        if($scope.reverse){
            $('#sortImg').remove();
            $(this).append('<span id="sortImg"> &#9650;</span>');}
        else{
            $('#sortImg').remove();
            $(this).append('<span id="sortImg"> &#9660;</span>');}
    });

} 
// function hide message box
hideMessbox = function(element){
  $(element).css("display", "none");
}
// ========== End Store list controller

//Gallery DETAIL CONTROLLER
function StoreDetailController($scope, $routeParams){
    $scope.uuid = $routeParams.storeid;
    $scope.store={};
    console.log(storeDeta, $scope.uuid);
    $scope.init = function(){
        $('#myModal').show();
      if(storeDeta != null){
        $scope.store = JSON.parse(JSON.stringify(storeDeta));
      }else{
        var options={
            method: "GET",
            endpoint: "stores/" + $scope.uuid
        }

        client.request(options, function(err, data){
            $('#myModal').hide();
            if(err)
                console.log("Could not get stores");
            else{
                $scope.store=data.entities[0];
                $scope.$apply($scope.store);
            }
        });
      }
    }    

    //Navigate to Users menu
    $scope.cancel = function(){
        location.href = "#/admin/";
    };
    
    $scope.update=function(uuid){
        $('#myModal').show();
        var options={
            method: 'PUT',
            endpoint: 'stores/'+ uuid,
            body:{
                title : $scope.store.title,
                name    : $scope.store.name,
                locationname    : $scope.store.locationname,
                latitude   : $scope.store.latitude,
                description : $scope.store.description,
                longtitude    : $scope.store.longtitude
            }
        };
        client.request(options, function(err,data){
            $('#myModal').hide();
            if(err)
                console.log('Could not update stores');
            else{
                location.href = "#/admin/";
            }
        });
    }

    $scope.resetStore = function(){
       $scope.init();
    }

   $(document).ready(function() {
      $scope.init();
    });
}