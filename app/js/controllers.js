'use strict';

/* Controllers */

var client = new Usergrid.Client({
    orgName:"binhnguyen",
    appName:"magrabbit"
});


var current_user;
 // Login controller
function LoginController($scope,$cookieStore) {
  $scope.current_user=$cookieStore.get('current_user');

  if($scope.current_user === null || $scope.current_user === undefined){
     location.href ="http://localhost:8000/#/login";
   }else{
     location.href ="http://localhost:8000/#/admin"; 
   }
    $scope.username = "";
    $scope.email = "binh@gmail.com";
    $scope.fullname = "";
    $scope.password = "";
    $scope.repassword = "";
    $scope.activated = "true";

  $scope._login = function(user) {
    console.log("login = "+ user.username);
    var username = user.username;
    var password = user.password;

    client.login(username, password, function (err, data, user) {
      if (err) {
        console.log("Error");
      } else {
        current_user = user;
        $cookieStore.put('current_user',username);
        location.href ="http://localhost:8000/#/admin";
      }
    });
  }

    $scope.submit = function(){
        console.log("username = "+ $scope.username);
        var options = {
            method:'POST',
            endpoint:"users",
            body:{name: $scope.fullname, username: $scope.username, email: $scope.email,
            password: $scope.password, activated: $scope.activated }
        };

        if($scope.password === $scope.repassword){
            client.request(options, function (err, data) {
                if (typeof(callback) === 'function') {
                  callback(err, data);
                }
                current_user = data.entities[0];
                location.href ="http://localhost:8000/#/books";
            });
        }else{
            alert("Error");
        }
    }

} // end login controller

// User controller
//var sdk = require('usergrid-sdk');
function BookController($scope, $http,$cookieStore ) {
  $scope.books =[];
  $scope.uuid = "";
  $scope.bookName ="";
  $scope.author ="";
  $scope.bookObject={};
  $scope.current_user=$cookieStore.get('current_user');
    $scope.getBooks = function() {
        console.log(current_user);
        var url ="";
        if(current_user != null){
            url = "users/" +  current_user._data.name + "/has/books";
          var options={
                method: 'GET',
                endpoint: url
              }
            client.request(options, function(err, data){
                if(err){
                  $("#msg").html('<pre>ERROR: '+data+'</pre>');
              } else {
                  $scope.books = data.entities;
                  $scope.$apply($scope.books);
              }
          });
      }else{
          url = "https://api.usergrid.com/binhnguyen/magrabbit/books";
          $http.get(url).success(function(data) {
              $scope.books = data.entities;
            });
      }
      var output = JSON.stringify($scope.books, null, 2);
      $("#response").html('<pre>'+output+'</pre>');

       $scope.dogs = new Usergrid.Collection({ "client":client, "type":"books" });

            $scope.dogs.fetch(function() {
               display();
            },function () {
                alert('error');
            }
        );
    }

    var display = function(){
        $('#next-button').hide();
        $('#previous-button').hide();
        $('#response').empty();
        while($scope.dogs.hasNextEntity()) {
            var dog = $scope.dogs.getNextEntity();
            $('#response').append('<li>'+ dog.get('name') + '</li>');
        }
        if ($scope.dogs.hasNextPage()) {
            $('#next-button').show();
        }
        if ($scope.dogs.hasPreviousPage()) {
            $('#previous-button').show();
        }
    }

  $scope.addBook = function(bookName, author) {
    console.log("bookName = " + bookName);
    console.log("author = " + author);
    var options = {
        type:"books"
        //qs:{ql:'order by title desc'}
    };

    client.createEntity(options, function(error, obj){
        if(error)
          alert("Error!!!");
        else{
          obj.set("name", bookName);
          obj.set("author", author);
          obj.save();
          var endpoint = current_user.type + '/' + current_user.name + '/' + "has" + '/' + obj.get("type") + '/' + obj.get("name");
          var options = {
            method:'POST',
            endpoint:endpoint
          };
          client.request(options, function (err, data) {
              if (err && client.logging) {
                console.log('entity could not be connected');
              }
              if (typeof(callback) === 'function') {
                callback(err, data);
              }
            $scope.getBooks();
          });
        }
    });
  }

  $scope.deleteBook = function(book) {
    console.log(book.uuid);
    var options={
      method: 'DELETE',
      endpoint:"books/" + book.uuid
    }
    client.request(options, function(err, data){
      if(err){
        $("#msg").html('<pre>ERROR: '+data+'</pre>');
      } else {
         $("#msg").html('<pre>'+ book.name +' has deleted :d</pre>');
         $scope.getBooks();
      }
    });
  }

  $scope.selectedBook = function(book) {
    $scope.uuid = book.uuid;
    $scope.bookName =book.name;
    $scope.author =book.author;
    $scope.bookObject = book;
  }

  $scope.prev = function() {
    $scope.dogs.getPreviousPage(function(err){
        if(err) {
            alert("ERROR");
        }else {
           display();
        }
    });
  }
  $scope.next = function() {
    $scope.dogs.getNextPage(function(err){
        if(err) {
            alert("ERROR");
        }else {
           display();
        }
    });
  }

  $scope.updateBook = function(bookName, author, uuid) {
    var options = {
        method:'PUT',
        endpoint:'books/' + uuid,
        body:{author: author }
    };
    client.request(options, function (err, data) {
        if (err) {
            alert("Error");
        } else {
           $scope.getBooks();
        }
    });
  }
} // end Book controller

//test for service
function TestController($scope) {
    $scope.isDisabled = false;
    $scope.names = ['igor', 'misko', 'vojta'];
    $scope.template = "partials/template1.html";
    $scope.template2 = "partials/template2.html";

    $scope.list = [];
    $scope.text = 'hello';
  $scope.submit = function() {
      if (this.text) {
          this.list.push(this.text);
          this.text = '';
      }
  }

  $scope.numbers = [1,2,3,4,5,6,7,8,9];
  $scope.limit = 3;

} // end Test controller



//test for service
function HelloCtrl($scope, testService, testFactory)
{
    $scope.name = "Binh Nguyen";
    $scope.fromService = testService.sayHello($scope.name);
    $scope.fromFactory = testFactory.sayHello("World");
    $scope.fromService1 = testService.sayGoodbye($scope.name);
    $scope.fromFactory1 = testFactory.sayGoodbye("World");
}

  $(document).ready(function() {
    if(current_user != null){
        $('#username').text = current_user.name;
        console.log(current_user.name);
    }

    var click = function(){
       console.log("fdsafdsa");
    }
      $('#aaa').click(function(){
        alert('jfhjdd');

      });
  });