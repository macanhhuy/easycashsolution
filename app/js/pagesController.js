'use strict';

var urlCommon ="http://localhost:8000/#/";
var page_current = null;
var menu_current = null;

function PageController($scope,$cookieStore) {
    $scope.pages =[];
    $scope.title   = "";
    $scope.name       = "";
    $scope.description = "";
    $scope.created   = "";
    $scope.modified   = "";
    $scope.status   = "";
    $scope.picture   = "";


    //Get all user
    var options={
        type: "pages",
        qs:{limit:10}
    };
    $scope.init = function(){
        $('#myModal').show(); 
        $scope.pages = [];
        var data;
        client.createCollection(options, function(err, data){
            if(err){
                console.log("Could not get user");
                $('#myModal').hide();
            }else{
                $scope.drawPages2 = function(){
                    data.fetch(function(err, d){
                        $('#prevPage').hide();
                        $('#nextPage').hide();

                        //reset the pointer
                        data.resetEntityPointer();
                        $scope.pages= data._list;
                        $scope.$apply($scope.pages);

                        if(data.hasNextPage())
                          $('#nextPage').show();
                        if(data.hasPreviousPage())
                            $('#prevPage').show();

                        $('#myModal').hide();
                        $scope.checkAll(false);
                    });
                } //End drawUser function

                $scope.prevPage = function(){
                    data.getPreviousPage(function(err, data){
                        if(err)
                            alert("err");
                        else
                            $scope.drawPages2();
                    });
                }
                $scope.nextPage = function(){
                    data.getNextPage(function(err, data){
                        if(err)
                            alert("err");
                        else
                            $scope.drawPages2();
                    });
                };
                $scope.drawPages2();
            };//End else
        });
    };

    $scope.search = '';
    $scope.filterByTitle = function(data){
      return data._data.title.toLowerCase().search($scope.search.toLowerCase()) != -1;
    }

    $scope.check = true;
    $scope.checkAll = function(data){
      if(data)
      $('input.chk').prop('checked', true);
      else
      $('input.chk').prop('checked', false);
    }
    $scope.check1 = function(){
      var d=false;
      $("input.chk:not(:checked)").each(function() {
                  d=true;
        });
        if(d){
           $('input.checkAll').prop('checked', false);
           $scope.check = true;
        }
        else{
          $('input.checkAll').prop('checked', true);
          $scope.check = false;
        }
    }
    
   
  $scope.open = function(){
     location.href="#/admin/createpage";
  };

  //"Create new user" pop-up disappears
  $scope.close = function(){
    $scope.shouldBeOpen = false;
  }

    $scope.cancel = function(){
        location.href = "#/admin";
    };

    //Delete pages function
  $scope.deletePage = function(){
     var array = [];
      $(".chk:checked").each(function(){
          array.push( $(this).val());
      });
        if(array.length > 0){
            if (confirm('Are you sure to delete?')){
                for(var i =0; i< array.length; i++){
                    var endpoint='pages/' + array[i] ;
                    var options={
                      method:'DELETE',
                      endpoint:endpoint
                    };
                    client.request(options, function(err, data){
                      if(err)
                         showMessCom('#messPageList', '<div class="alert alert-error">'+data.error_description +'</div>');
                      else{
                        location.reload();
                      }
                    });
                }
            }
        }else{
           showMessCom('#messPageList', '<div class="alert alert-error">Please select at least a row to delete.</div>');
        }  
   };

  //Create new user function
  $scope.createPage = function(){
    if($scope.title.trim() !== ""){
        var options ={
            method : "POST",
            endpoint: "pages",
            body: {
                title : $scope.title,
                name : $scope.title,
                description : $scope.description,
                status: $scope.status,
                picture: $scope.picture
            }
        };

        client.request(options, function(err, data){
            if(err){
                $('#messPageAdd').html('<div class="alert alert-error"> Could not create page, '+ data.error_description + '!</div>');
                $('#messPageAdd').css("display","block");
            }
            else{
                $scope.shouldBeOpen = false;
                location.reload();
            }
        });
    }else{
        $('#messPageAdd').html('<div class="alert alert-error"> Input data invalid, title field is required!</div>');
        $('#messPageAdd').css("display","block");
    }

  };

  $scope.pageDetail = function(obj){
    page_current = obj;
  };

    $scope.resetPage = function(){
      $scope.title = "";
      $scope.name = "";
      $scope.description = "";
      $scope.status = 0;
      $scope.picture = "";
    };

    $scope.predicate = '_data.name';
    $('.sort').click(function(){
        if($scope.reverse){
            $('#sortImg1').remove();
            $(this).append('<span id="sortImg1"> &#9650;</span>');}
        else{
            $('#sortImg1').remove();
            $(this).append('<span id="sortImg1"> &#9660;</span>');}
    });
}

//USER DETAIL CONTROLLER
function PageDetailController($scope, $routeParams){
    $scope.uuid = $routeParams.pageid;
    $scope.page={};

    $scope.init = function(){
      if(page_current != null){
        $scope.page = JSON.parse(JSON.stringify(page_current));
      }else{
        var options={
          method: "GET",
          endpoint: "pages/" + $scope.uuid
        }

        client.request(options, function(err, data){
            if(err){
                showMess('<div class="alert alert-error">'+ data.error_description +'</div>');
            }
            else{
                $scope.page=data.entities[0];
                $scope.$apply($scope.page);
                CKEDITOR.instances.editor.setData($scope.page.description);
               }
         });
      }
    }

    $scope.cancel = function(){
        location.href = "#/admin";
    };

    //Update Page function
    $scope.update=function(uuid){
        console.log($scope.page.status);
      $scope.page.description = CKEDITOR.instances.editor.getData();
        if($scope.page.title !== undefined){
            var options={
                method: 'PUT',
                endpoint: 'pages/'+ uuid,
                body:{
                    title : $scope.page.title,
                    description    : $scope.page.description,
                    status    : $scope.page.status,
                    picture  : $scope.page.picture
                }
            };
            client.request(options, function(err,data){
                if(err){
                    showMess('<div class="alert alert-error">'+ data.error_description + '</div>');
                }
                else{
                    location.href = "#/admin";
                }
            });
        }else{
            showMess('<div class="alert alert-error"> Input data invalid, title field is required!</div>');
        }
    }

    var showMess = function(mess){
        $('#messPageDetail').html(mess);
        $('#messPageDetail').css("display","block");
    }

    $scope.resetPage = function(){
      if(page_current!= null){
         $scope.page = page_current;
      }
    }

    $(document).ready(function() {
      $scope.init();
      $('#editor').ckeditor();
      CKEDITOR.config.skin = 'moonocolor';
    });
}

function PageCreateController($scope){
    $scope.page={};
    $scope.page.status= 0;
    //Create new user function
    $scope.save = function(page){
        if(page.title !== undefined){
            var options ={
                method : "POST",
                endpoint: "pages",
                body: {
                    title : page.title,
                    name : page.title,
                    description : page.description,
                    status: page.status,
                    picture: page.picture
                }
            };

            client.request(options, function(err, data){
                if(err){
                    showMessCom('#messPageAdd', '<div class="alert alert-error">'+ data.error_description + '</div>');
                }
                else{
                    location.href = "#/admin";
                }
            });
        }else{
            showMessCom('#messPageAdd', '<div class="alert alert-error">Input data invalid, title field is required!</div>');
        }
    }

    $scope.resetPage = function(){
        $scope.page={};
        $scope.page.status = 0;
        $scope.page.description = "";
    }

    $scope.cancel = function(){
        location.href = "#/admin";
    };

    $(document).ready(function() {
      $('#editor').ckeditor();
      CKEDITOR.config.skin = 'moonocolor';
    });

}

//Menu CONTROLLER
function MenusController($scope){

    $scope.menus =[];
    $scope.title   = "";
    $scope.name       = "";
    $scope.order = 0;
    $scope.created   = "";
    $scope.modified   = "";
    $scope.parrent   = "";
    $scope.path   = "";

  
    var options={
        type: "menus",
        qs:{limit:10}
    };
    $scope.init = function(){
         $('#myModal').show(); 
        $scope.menus = [];
        var data;
        client.createCollection(options, function(err, data){
           
            console.log(data);
            if(err){
                console.log("Could not get user");
            }else{
                $scope.drawPages2 = function(){
                    data.fetch(function(err, d){
                        $('#prevPage').hide();
                        $('#nextPage').hide();

                        //reset the pointer
                        data.resetEntityPointer();
                        $scope.menus= data._list;
                        $scope.$apply($scope.menus);

                        if(data.hasNextPage())
                          $('#nextPage').show();
                        if(data.hasPreviousPage())
                            $('#prevPage').show();
                          $scope.checkAll(false);
                    });
                } //End drawUser function

                $scope.prevPage = function(){
                    data.getPreviousPage(function(err, data){
                        if(err)
                            alert("err");
                        else
                            $scope.drawPages2();
                    });
                }
                $scope.nextPage = function(){
                    data.getNextPage(function(err, data){
                        if(err)
                            alert("err");
                        else
                            $scope.drawPages2();
                    });
                };
                $scope.drawPages2();
            }//End else
             $('#myModal').hide();
        });
    };

    $scope.search = '';
    $scope.filterByTitle = function(data){
      return data._data.title.toLowerCase().search($scope.search.toLowerCase()) != -1;
    }

    $scope.check = true;
    $scope.checkAll = function(data){
      if(data)
      $('input.chk').prop('checked', true);
      else
      $('input.chk').prop('checked', false);
    }
    $scope.check1 = function(){
      var d=false;
      $("input.chk:not(:checked)").each(function() {
                  d=true;
        });
        if(d){
           $('input.checkAll').prop('checked', false);
           $scope.check = true;
        }
        else{
          $('input.checkAll').prop('checked', true);
          $scope.check = false;
        }
    }
    
    //"Create new user" pop-up appears
    $scope.open = function(){
        //$scope.shouldBeOpen = true;
        location.href="#/admin/createmenu";
    };

    //"Create new user" pop-up disappears
    $scope.close = function(){
      $scope.shouldBeOpen = false;
    }

    $scope.cancel = function(){
        location.href = "#/admin";
    }

    //Delete pages function
  $scope.deleteMenu = function(){
     var array = [];
      $(".chk:checked").each(function(){
          array.push( $(this).val());
      });
       if(array.length > 0){
            if (confirm('Are you sure to delete?')){
                for(var i =0; i< array.length; i++){
                    var endpoint='menus/' + array[i] ;
                    var options={
                      method:'DELETE',
                      endpoint:endpoint
                    };
                    client.request(options, function(err, data){
                      if(err)
                        showMessCom('#messMenuList', '<div class="alert alert-error">'+ data.error_description +'</div>');
                      else{
                        location.reload();
                      }
                    });
                }
            }
        }else{
           showMessCom('#messMenuList', '<div class="alert alert-error">Please select at least a row to delete.</div>');
        } 
   }

    //Create new user function
    $scope.createMenu = function(){
        var options ={
          method : "POST",
          endpoint: "menus",
          body: {
            title : $scope.title,
            name : $scope.title,
            path : $scope.path,
            order : $scope.order,
            parrent : $scope.parrent
          }
        };

        client.request(options, function(err, data){
          if(err)
            showMessCom('#messagebox', '<div class="alert alert-error">' + data.error_description + '</div>');
          else{
            $scope.shouldBeOpen = false;
            location.reload();
          }
        });
    }

  $scope.menuDetail = function(obj){
    menu_current = obj;
  }

    $scope.predicate = '_data.name';
    $('.sort').click(function(){
        if($scope.reverse){
            $('#sortImg').remove();
            $(this).append('<span id="sortImg"> &#9650;</span>');}
        else{
            $('#sortImg').remove();
            $(this).append('<span id="sortImg"> &#9660;</span>');}
    });

  $(document).ready(function(){
    $scope.init();
  });

} // End Menu List controller

//MENU DETAIL CONTROLLER
function MenuDetailController($scope, $routeParams){
    $scope.uuid = $routeParams.menuid;
    $scope.menu={};
    
    $scope.init = function(){
        console.log(menu_current, $scope.uuid);
      if(menu_current!= null){
         $scope.menu = JSON.parse(JSON.stringify(menu_current));
      }else{
        var options={
          method: "GET",
          endpoint: "menus/" + $scope.uuid
        }

        client.request(options, function(err, data){
            if(err)
                showMessCom('messMenuDetail', '<div class="alert alert-error">' + data.error_description + '</div>');
            else{
                $scope.menu=data.entities[0];
                $scope.$apply($scope.menu);
            }
        });
      }
    }

    //Update User function
    $scope.update=function(uuid){
        var options={
            method: 'PUT',
            endpoint: 'menus/'+ uuid,
            body:{
                title : $scope.menu.title,
                name    : $scope.menu.name,
                path    : $scope.menu.path,
                order   : $scope.menu.order,
                parrent : $scope.menu.parrent
            }
        };
        client.request(options, function(err,data){
            if(err)
               showMessCom('messMenuDetail', '<div class="alert alert-error">' + data.error_description + '</div>');
            else{
                location.href = "#/admin";
            }
        });
    }

    $scope.resetMenu = function(){
        $scope.init();
    }

    $scope.cancel = function(){
        location.href="#/admin";
    }

    $(document).ready(function() {
      $scope.init();
    });
}

//CREATE MENU CONTROLLER
function CreateMenuController($scope){
    $scope.menu={};
    $scope.menus = [];
    $scope.categories=[];
    $scope.pages=[];
    $scope.titleCustom ="";
    $scope.urlCustom ="";

    $scope.checkPage =false;
    $scope.checkCate =false;

    $scope.getCategories = function(){
        var options={
            type: "categories"
        }

        client.createCollection(options, function(err, data){
            if(err){
               showMessCom('#messagebox', '<div class="alert alert-error">'+ data.error_description +'</div>');
                return;
            }else{
                $scope.categories= data._list;
                $scope.$apply($scope.categories);
            }
        });
    }

    $scope.getAllPages = function(){
        var options={
            type: "pages"
        }

        client.createCollection(options, function(err, data){
            if(err){
                showMessCom('#messagebox', '<div class="alert alert-error">'+ data.error_description +'</div>');
                return;
            }else{
                $scope.pages= data._list;
                $scope.$apply($scope.pages);
            }
        });
    }

    $scope.addLinkToMenu = function(){
        var newMenu = {};
        newMenu.title = $scope.titleCustom;
        newMenu.name = $scope.titleCustom;
        newMenu.path = "#/" + $scope.urlCustom;
        newMenu.order = 1;
        newMenu.parrent = 1;
        $scope.createMenu(newMenu);
    }

    $scope.addPageToMenu = function(pages){
        for(var p in pages){
            if(pages[p]._data.ischecked == true){
                var newMenu = {};
                newMenu.title = pages[p]._data.title;
                newMenu.name = pages[p]._data.title;
                newMenu.path ="#/" + pages[p]._data.title;
                newMenu.order = 1;
                newMenu.parrent = 1;
                $scope.createMenu(newMenu);
            }

        }
    }

    $scope.addCategoryToMenu = function(categories){
        for(var p in categories){
            if(categories[p]._data.ischecked == true){
                var newMenu = {};
                newMenu.title = categories[p]._data.title;
                newMenu.name = categories[p]._data.title;
                newMenu.path ="#/" + categories[p]._data.title;
                newMenu.order = 1;
                newMenu.parrent = 1;
                $scope.createMenu(newMenu);
            }

        }
    }

    $scope.init = function(){
        $scope.getCategories();
        $scope.getAllPages();
        $scope.getAllMenus();
    }

    $scope.getAllMenus = function(){
        var options={
            type: "menus"
        }

        client.createCollection(options, function(err, data){
            if(err){
                showMessCom('#messagebox', '<div class="alert alert-error">'+ data.error_description +'</div>');
                return;
            }else{
                $scope.menus= data._list;
                $scope.$apply($scope.menus);
                $('#loading').css("display", "none");
            }
        });
    }

    $scope.createMenu = function(newMenu){
        $('#loading').css("display", "block");
        var options ={
            method : "POST",
            endpoint: "menus",
            body: {
                title : newMenu.title,
                name : newMenu.name,
                path : newMenu.path,
                order : newMenu.order,
                parrent : newMenu.parrent
            }
        };

        client.request(options, function(err, data){
            if(err){
                showMessCom('#messagebox', '<div class="alert alert-error">'+ data.error_description +'</div>');
            }
            else{
                showMessCom('#messagebox', '<div class="alert alert-success"> Create new menu successfuly.</div>');
                $scope.getAllMenus();
            }
        });
    }

    $scope.updateMenu = function(menu){
        $('#loading').css("display", "block");
        var options ={
            method : "PUT",
            endpoint: "menus/" + menu.uuid,
            body: {
                title : menu.title,
                path : menu.path,
                order : menu.order,
                parrent : menu.parrent
            }
        };

        client.request(options, function(err, data){
            if(err){
              showMessCom('#messagebox', '<div class="alert alert-error">'+ data.error_description +'</div>');
            }
            else{
                $scope.getAllMenus();
            }
        });
    }

    $scope.deleteMenu = function(menu){
        console.log(menu);
        $('#loading').css("display", "block");
        var options ={
            method : "DELETE",
            endpoint: "menus/" + menu.uuid
        };

        client.request(options, function(err, data){
            if(err)
                showMessCom('#messagebox', '<div class="alert alert-error">'+ data.error_description +'</div>');
            else{
                $scope.getAllMenus();
            }
        });
    }

    $scope.selectAllCategories = function(checked){
        for(var p in $scope.categories){
            $scope.categories[p]._data.ischecked = checked;
        }
    }

    $scope.selectAllPages = function(checked){
        for(var p in $scope.pages){
            $scope.pages[p]._data.ischecked = checked;
        }
    }

    $scope.resetMenu = function(){
      $scope.menu={};
    }

    $(document).ready(function() {
        $('#loading').css("display", "block");
        $scope.init();
    });
}

var showMessCom = function(element, mess){
    $(element).html(mess);
    $(element).css("display","block");
}
