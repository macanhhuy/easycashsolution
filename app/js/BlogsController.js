function BlogsController($scope,$cookieStore){

	document.title = 'Blog';
	$scope.search = '';
  	$scope.filterByTitle = function(data){
		return data._data.title.toLowerCase().search($scope.search.toLowerCase()) != -1;
	}

	//ALERT
	$scope.openAlert = function(type, title, message){
  		$('#alertMsg').html('<div class="alert alert-'+type+'"><strong>'+title+'</strong> '+message+'</div>').show('slow');  		
  		setTimeout(function(){$('#alertMsg').hide('slow')},4000);
  	};

    var options = {
      type: "blogs"
    };
    
	$scope.init = function(){
		$('#myModal').show();
        $scope.contents = [];
        var data;
        client.createCollection(options, function(err, data){
            if(err){
               //showMess('#messLisImg', '<div class="alert alert-error"><b>Error</b> Could not get list Stores!</div>');
                $('#myModal').hide();
            }else{
                $scope.drawPages2 = function(){
                    data.fetch(function(err, d){
                        $('#prevPage').hide();
                        $('#nextPage').hide();

                        //reset the pointer
                        data.resetEntityPointer();
                        $scope.contents= data._list;
                        console.log($scope.contents);
                        $scope.$apply($scope.contents);

                        if(data.hasNextPage())
                          $('#nextPage').show();
                        if(data.hasPreviousPage())
                            $('#prevPage').show();

                        $('#myModal').hide();
                    });
                } //End drawUser function

                $scope.prevPage = function(){
                    data.getPreviousPage(function(err, data){
                        if(err)
                            alert("err");
                        else
                            $scope.drawPages2();
                    });
                }
                $scope.nextPage = function(){
                    data.getNextPage(function(err, data){
                        if(err)
                            alert("err");
                        else
                            $scope.drawPages2();
                    });
                };
                $scope.drawPages2();
            };//End else
        });
    };

    $scope.create = function(){
    	$cookieStore.remove('current_obj');
    	location.href="#/admin/createblogs";
    }
    $scope.array = [];
    $scope.delete = function(){
    	$(".chk:checked").each(function(){
			    $scope.array.push( $(this).val());
			});
    	if($scope.array.length == 0){
    		$scope.openAlert("error","","Please select at least a row to delete.");
    	}
    	else
		if (confirm('Are you sure you want to delete?')){
			$('#myModal').show();
			for(var i =0; i< $scope.array.length; i++){
				var endpoint='blogs/' + $scope.array[i] ;
				var options={
					method:'DELETE',
					endpoint:endpoint
				};
				client.request(options, function(err, data){
					if(err){
						 $scope.openAlert("error","","Could not delete blog.");
					}
					else{
						$('#myModal').hide();
						$scope.init();
						$scope.openAlert("success","","Deleted Successfully.");
					}
				});
			}

		}
	};
  	  	// sort
  	$scope.predicate = 'title';
  	$('.sort').click(function(){
  		if($scope.reverse){
  			$('#sortImg').remove();
  			$(this).append('<span id="sortImg"> &#9650;</span>');}
  		else{
  			$('#sortImg').remove();
  			$(this).append('<span id="sortImg"> &#9660;</span>');}
  		});
  	$scope.checkDelete = true;
  	$(document).ready(function(){
		$(".chk").change(function(e){
		    if ($(this).is(":checked"))
		        alert("checked");
		    else
		        alert("not checked");
			});
  	});
  	$scope.check = true;
  	$scope.checkAll = function(data){
  		console.log(data)
  		if(data)
  		$('input.chk').prop('checked', true);
  		else
  		$('input.chk').prop('checked', false);
  	}
  	$scope.check1 = function(){
  		var d=false;
  		$("input.chk:not(:checked)").each(function() {
                  d=true;
        });
        if(d){
       		 $('input.checkAll').prop('checked', false);
       		 $scope.check = false;
        }
        else{
        	$('input.checkAll').prop('checked', true);
        	$scope.check = true;
        }
  	}
}// end BlogController 

function CreateBlogsController($scope,$cookieStore,$http){
	//$scope.word = /^\w*$/;
	$scope.word = /^([0-9a-zA-Z ])*$/;
	$('#myModal').show();
	$('#editor').ckeditor();
	CKEDITOR.config.skin = 'moonocolor';
	current_user = $cookieStore.get("current_user");
	$scope.descriptions ="";
	$scope.status = "";
	$scope.title = "";
	$scope.categorie =""
	$scope.uuid = "";
	$scope.selected = "";

	$scope.openAlert = function(type, title, message){
  		$('#alertMsg').html('<div class="alert alert-'+type+'"><strong>'+title+'</strong> '+message+'</div>').show('slow');  		
  		setTimeout(function(){$('#alertMsg').hide('slow')},4000);
  	};

	var options1={
		method: "GET",
		endpoint: "categories/"
	}
	$scope.init = function(){

		$scope.categories = [];
		client.request(options1, function(err, data){
			if(err){
				$('#myModal').hide();
				console.log("Could not get user");
				alert('could not get categories from server');
				location.reload();
			}
			else{
				$scope.categories =  data.entities;
				$scope.$apply($scope.categories);
				// $scope.selected = $scope.categories[0].name;
				// $scope.$apply($scope.selected);
				$('#myModal').hide();
			}
		});
	}

	$scope.createBlogs = function(){
		$('#myModal').show();
		var name = $scope.title.replace(/\s/g, "");
		var category = $('#select option:selected').text();
		var categoryuuid = $('#select option:selected').val();
		var descriptions = $('#editor').val();
		$(".chk:checked").each(function(){
			$scope.status = $(this).val();
		});

	    var options = {
	        method:'POST',
	        endpoint: 'blogs',
	        body:{
	        	name: name,
	        	title:$scope.title,
	        	author: current_user,
	        	status:$scope.status,
	        	descriptions:descriptions,
	        	categories:category
	        }
	    };
	          client.request(options, function (err, obj) {
	              if (err) {
	              	$('#myModal').hide();
	                $scope.openAlert("error","","Could not create blog");
	              }else{
	              	var endpoint = "categories" + '/' + $scope.cate + '/' + "has" + '/' + "blogs" + '/' + name;
	              	var options2= {
	            		method:'POST',
	           		 	endpoint:endpoint
	          		};
	          		client.request(options2, function(err, data){
						if(err){
							$scope.openAlert("error","","Could not create relationship");
							//if could not create relationship ,delete blogs 
							$scope.deleteBlog(name);
							$('#myModal').hide();
						}
						else{
							$('#myModal').hide();
							location.href = "#/admin";
						}// ense else
					});
	              } // end else
	          });
	}// end function createBlogs

	$scope.cancel = function(){
		location.href = "#/admin";
	};


}
// End  DetaisController
function UpdateBlogsController($scope, $routeParams, $cookieStore){
	$scope.word = /^([0-9a-zA-Z ])*$/;
	$('#myModal').show();
	var old_category = "";
	var old_categoryuuid = "";
	$('#editor').ckeditor();
	CKEDITOR.config.skin = 'moonocolor';
	$scope.selected = {};
	$scope.category = null;
	$scope.categoryuuid = null;
	$scope.uuid = $routeParams.blogsid;
	$scope.contents={};

	$scope.openAlert = function(type, title, message){
  		$('#alertMsg').html('<div class="alert alert-'+type+'"><strong>'+title+'</strong> '+message+'</div>').show('slow');  		
  		setTimeout(function(){$('#alertMsg').hide('slow')},4000);
  	};

	var options={
		method: "GET",
		endpoint: "blogs/" + $scope.uuid
	}
	client.request(options, function(err, data){
			if(err){
				
				alert("Could not get blog");
				location.href="#/admin";}
			else{
				$scope.contents=data.entities[0];
				old_category = data.entities[0].categories;
				$('#editor').val($scope.contents.descriptions);
				$('input:radio[value="' + $scope.contents.status +'"]').attr('checked', 'checked'); // assign status of current blog
				$scope.$apply($scope.contents);
			}
	});
	var options1={
		method: "GET",
		endpoint: "categories/"
	}

	client.request(options1, function(err, data){
		if(err){
			alert('Could not get categories from server');
			location.reload();
			}
		else{
		//	$('#myModal').hide();
			$scope.categories =  data.entities;
			$scope.selected = data.entities[0];
			$.each(data.entities, function(key, value) {
			 	if(value.name === $scope.contents.categories){
			 		$scope.selected = data.entities[key];
			 		old_categoryuuid = data.entities[key].uuid;
			 	}
			});
			$scope.$apply($scope.selected);
			$scope.$apply($scope.categories);
		}
	});
		$scope.category = $scope.selected.name;
		$scope.categoryuuid = $scope.selected.uuid;

	//Navigate to Users page
	$scope.cancel = function(){
		location.href = "#/admin";
	};

	//Update User function
	$scope.update=function(uuid){
			$('#myModal').show();
			if(old_category != $scope.selected.name){ // if change category
				var endpoint='categories/' + old_categoryuuid + '/has/blogs/' + $scope.contents.uuid;  
				var options={
					method:'DELETE',
					endpoint:endpoint
				};
				client.request(options, function(err, data){
					if(err){
						$('#myModal').hide();
						$scope.openAlert("error","","Could not update");
					}
					else{
						
					}
				});
			}
			var blogname = $scope.contents.title.replace(/\s/g, "");
			var descriptions = $('#editor').val();
			$(".chk:checked").each(function(){
			    $scope.status = $(this).val();
			});
		    var options = {
		        type:"blogs/" + $scope.contents.uuid
		    };
	    	client.createEntity(options, function(error, obj){
	        if(error)
	          alert("Error!!!");
	        else{
	          obj.set("title", $scope.contents.title);
	          obj.set("author",current_user);
	          obj.set("status",$scope.status);
	          obj.set("descriptions",descriptions);
	          obj.set("categories",$scope.selected.name);
	          obj.save();
	          var options = {
	            method:'PUT'
	          };
	          client.request(options, function (err, data) {
	              if (err && client.logging) {
	              	$('#myModal').hide();
	              	$scope.openAlertFail();
	              }else{
	              	var endpoint = "categories" + '/' + $scope.selected.uuid + '/' + "has" + '/' + obj.get("type") + '/' + $scope.contents.uuid;
	              	var options={
					method:'POST',
					endpoint:endpoint
					};
					client.request(options, function(err, data){
					if(err){
						$('#myModal').hide();
						$scope.openAlert("error","","Could not create relationship");
					}
					else{
						$('#myModal').hide();
						location.href = "#/admin";
					}
					});
	              }
	         });
	    }
	});
	};//end function update
};