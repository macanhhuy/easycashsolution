//=========USER CONTROLLER=========
function UserController($scope) {
	document.title = 'User Management';
	
	//Check, Uncheck all checkbox
	$scope.check = false;
  	$scope.checkAll = function(){
  		if($scope.check)
  			$('input.chk').prop('checked', true);
  		else
  			$('input.chk').prop('checked', false);
  	}
  	$scope.checkItem = function(){
  		var ok=false;
  		$("input.chk:not(:checked)").each(function() {ok=true;});
        if(ok){        	
       		$('input.checkAll').prop('checked', false);
       		$scope.check = false;
        }
        else{
        	$('input.checkAll').prop('checked', true);
        	$scope.check = true;
        }
  	}//End check, uncheck checkbox
	//Initializing information about User
	$scope.username   = "";
	$scope.name       = "";
	$scope.email      = "";
	$scope.password   = "";
	$scope.repassword = "";
	$scope.users      = [];
	
	//Regular Expression
	$scope.usernameRegex = /^\w*$/;
	$scope.nameRegex = /^([0-9a-zA-Z ])*$/;

	//Pagination
	// $scope.pages =[];
 //    $scope.currentPage = 0;
 //    $scope.pageSize = 10;
 //    $scope.numberOfPages=0;
 //    $scope.totalRecord=0;
  	// $scope.paging = function(index){
  	// 	$scope.currentPage=Math.ceil(index);
  	// }	

	

  	// //Get all user
	// var options={
	// 	type: "users",
	// 	qs:{limit: 999,ql:"order by username"}
	// }
	// $scope.init = function(){
	// 	$scope.users = [];
	// 	$scope.pages =[];
	// 	client.createCollection(options, function(err, data){
	// 		if(err){
	// 			console.log("Could not get user");
	// 		}else{
	// 			$scope.users= data._list;
	// 			$scope.$apply($scope.users);
	// 			$scope.totalRecord = $scope.users.length;
	// 			$scope.numberOfPages=Math.ceil($scope.users.length/$scope.pageSize);                
	// 			// $scope.$apply($scope.numberOfPages);
	// 			$scope.$apply($scope);
	// 			console.log($scope.totalRecord);
	// 			for(var i =1 ; i<=$scope.numberOfPages;i++){
	// 				$scope.pages.push(i);
	// 			}		
	// 			$scope.$apply($scope.pages);
	// 			$('#myModal').hide();
	// 		}//End else
	// 	});			

	// };
	//Get all user
    var options={
        type: "users",
        qs:{ql:"order by username"}
    };
    $scope.init = function(){
    	$('#myModal').show();
        $scope.users = [];
        var data;
        client.createCollection(options, function(err, data){
            if(err){            	
                $('#myModal').hide();
                $scope.openAlert("error","", "Could not get user.");
            }else{
                $scope.drawListUser = function(){
                    data.fetch(function(err, d){
                        $('#prevPage').hide();
                        $('#nextPage').hide();

                        //reset the pointer
                        data.resetEntityPointer();
                        $scope.users= data._list;
                        $scope.$apply($scope.users);                        

                        //if(data.hasNextPage() && data._previous.lenght!=0)
                        if(data.hasNextPage())
                        	$('#nextPage').show();

                        if(data.hasPreviousPage())
                            $('#prevPage').show();

                        $('#myModal').hide();
                    });
                } //End drawListUser function

                $scope.prevPage = function(){
                    data.getPreviousPage(function(err, data){
                        if(err)
                            $scope.openAlert("error","","Could not get previous page.");
                        else
                            $scope.drawListUser();
                    });
                }
                $scope.nextPage = function(){
                    data.getNextPage(function(err, data){
                        if(err)
                            $scope.openAlert("error","","Could not get next page.");
                        else
                            $scope.drawListUser();
                    });
                };
                $scope.drawListUser();
            };//End else
        });
    };

	//Initializing a pop-up
	$scope.opts = {
	    backdropFade: true,
	    dialogFade	: true
  	};

  	//"Create new user" pop-up appears
	$scope.open  = function(){
		$scope.username     = "";
		$scope.name         = "";
		$scope.email        = "";
		$scope.password     = "";
		$scope.repassword   = "";
		$scope.closeAlert();
		$scope.shouldBeOpen = true;
	};

	//"Create new user" pop-up disappears
	$scope.close = function(){ $scope.shouldBeOpen = false;};

	//Delete users function
	$scope.delete = function(){
		//get all checked checkbox
		var array = [];
		$(".chk:checked").each(function(){
		    array.push( $(this).val());
		});
		if(array.length<=0)
			$scope.openAlert("error","","Please select at least a row to delete.");
		else
			if (confirm('Are you sure you want to delete?')){
				for(var i =0; i< array.length; i++){
					var endpoint='users/' + array[i] ;
					var options={
						method:'DELETE',
						endpoint:endpoint
					};
					client.request(options, function(err, data){
						if(err)
							$scope.openAlert("error","","Could not delete user.");
						else{
							$scope.openAlert("success","","Deleted Successfully.");
							$scope.init();						
						}
					});
				}//end for
			}//end confirm
	};

	//Create new user function
	$scope.createUser = function(){
		var options ={
			method : "POST",
			endpoint: "users",
			body: {
				username : $scope.username,
				name : $scope.name,
				email : $scope.email,
				password : $scope.password
			}
		};
		$scope.shouldBeOpen = false;
		client.request(options, function(err, data){
			if(err){
				$scope.openAlert("error","","Could not create a user.");
			}
			else{
				$scope.init();
				$scope.openAlert("success","","Created Successfully.");
			}
		});
	};

	//Navigate to User detail page
	$scope.navDetail = function(user_uuid){
		location.href = "#/admin/users/" +  user_uuid;
	};

	//Sort
	$scope.predicate = 'username';
  	$('.sort').click(function(){
  		if($scope.reverse){
  			$('#sortImg').remove();
  			$(this).append('<span id="sortImg"> &#9650;</span>');}
  		else{
  			$('#sortImg').remove();
  			$(this).append('<span id="sortImg"> &#9660;</span>');}
  	});

  	//Filter
	$scope.search='';
	$scope.filterByUsername = function(data){
		return data._data.username.toLowerCase().search($scope.search.toLowerCase()) != -1;
	}

	//ALERT
	$scope.openAlert = function(type, title, message){
  		$('#alertMsg').html('<div class="alert alert-'+type+'"><strong>'+title+'</strong> '+message+'</div>').show('slow');  		
  		setTimeout(function(){$('#alertMsg').hide('slow')},4000);
  	};
  	$scope.closeAlert = function(){
  		$('#alertMsg').hide();
  	};
};
//=========END USER CONTROLLER=========

//=========USER DETAIL CONTROLLER=========
function UserDetailController($scope, $routeParams){ 	
	document.title = 'Detail User';
	$scope.uuid = $routeParams.userid;
	$scope.user={};
	$scope.groups_add_user=[];
	$scope.user_has_group =[];
	$scope.role_of_user =[];
	$scope.role_add_user =[];

	$scope.telephoneRegex = /^([0-9])*$/;

	//ALERT
	$scope.openAlert = function(type, title, message){
  		$('#alertMsg').html('<div class="alert alert-'+type+'"><strong>'+title+'</strong> '+message+'</div>').show('slow');  		
  		setTimeout(function(){$('#alertMsg').hide('slow')},3000);
  	};
  	$scope.closeAlert = function(){
  		$('#alertMsg').hide();
  	};  

	$scope.init=function(){
		var options={
			method: "GET",
			endpoint: "users/" + $scope.uuid
		};
		client.request(options, function(err, data){
			if(err)
				console.log("Could not get user");
			else{
				$scope.user=data.entities[0];
				$scope.$apply($scope.user);
			}
		});

		//List group of user
		options = {type: "users/" + $scope.uuid + "/groups"};
		client.createCollection(options, function(err, data){
			if(err)
				console.log("Could not get group!");
			else{
				$scope.user_has_group = data._list;
				$scope.$apply($scope.user_has_group);

				//Load list group add to user
				var options = {type : "groups"};

				client.createCollection(options, function(err, data){
					if(err)
						console.log("Could not get users");
					else{
						while(data.hasNextEntity()){
							var obj=data.getNextEntity();
							if(!isExistGroup(obj.get("path")))
								$scope.groups_add_user.push(obj.get("path"));
						}
					}
				});//end request
			}//end else
		});//end get list group of user
		//List role of user
		options = {type: "users/" + $scope.uuid + "/roles"};
		client.createCollection(options, function(err, data){
			if(err)
				console.log("Could not get role of user");
			else{
				$scope.role_of_user = data._list;
				$scope.$apply($scope.role_of_user);

				var options = { type : "roles" };

				client.createCollection(options, function(err, data){
					if(err)
						console.log("Could not get roles");
					else{
						$scope.role_add_user = [];
						while(data.hasNextEntity()){
							var obj=data.getNextEntity();
							if(!isExistRole(obj.get("roleName")))
								$scope.role_add_user.push(obj.get("roleName"));
						}
					}
				});//end request
			}//end else
		});//end list role of user
	};
	//---------- DETAIL TAB -----------
	//Navigate to Users page
	$scope.cancel = function(){
		location.href = "#/admin";
	};

	//Update User function
	$scope.update=function(uuid){
		var options={
			method: 'PUT',
			endpoint: 'users/'+ uuid,
			body:{
				username : $scope.user.username,
				name 	 : $scope.user.name,
				title 	 : $scope.user.title,
				url 	 : $scope.user.url,
				email 	 : $scope.user.email,
				tel 	 : $scope.user.tel,
				picture  : $scope.user.picture,
				bday 	 : $scope.user.bday
			}
		};
		client.request(options, function(err,data){
			if(err)
				$scope.openAlert("error","","Could not update user.");
			else{
				$scope.init();
				$scope.openAlert("success","","Updated Successfully.");
			}
		});
	};
	//---------- END DETAIL TAB -----------
	//---------- GROUP TAB -----------
	//Initializing a pop-up
	$scope.opts = {
	    backdropFade: true,
	    dialogFade:true
  	};

  	// "Add new group to user" pop-up appears
	$scope.openGroup = function(){
		$scope.closeAlert();
		$scope.popupGroup = true;
	};

	// "Add new group to user" pop-up disappears
	$scope.closeGroup = function(){
		$scope.popupGroup = false;
	};

	//Add group to group function
	$scope.addGroup = function(){
		//get all checked checkbox
		var arrGroup_add_user = [];
		$(".chkGroup_add_user:checked").each(function(){
		    arrGroup_add_user.push( $(this).val());
		});

		for(var i =0; i< arrGroup_add_user.length; i++){
			var options ={
				method : "POST",
				endpoint : "users/" + $scope.uuid + "/groups/" + arrGroup_add_user[i]
			};
		client.request(options, function(err, data){
			if(err)
				$scope.openAlert("error","","Could not add group.");
			else{
				$scope.popupGroup = false;
				$scope.init();
				$scope.openAlert("success","","Created Successfully.");
			}
		});
		}
	};

	//Remove group of user
	$scope.removeGroup = function(){
		//get all checked checkbox
		var arrGroups = [];
		$(".chkGroups:checked").each(function(){
		    arrGroups.push( $(this).val());
		});
		if(arrGroups.length<=0){
			$('input.checkallGroup').prop('checked', false);
			$scope.openAlert("error","","Please select at least a row to delete.");
		}
		else{
			if (confirm('Are you sure you want to delete?')){			
				for(var i = 0; i< arrGroups.length; i++){
					var endpoint = "users/" + $scope.uuid + "/groups/" + arrGroups[i];
					var options = {
						method:'DELETE',
						endpoint:endpoint
					};
					client.request(options, function(err, data){
						if(err)
							$scope.openAlert("error","","Could not delete group.");
						else{
							$scope.init();
							$('input.checkallGroup').prop('checked', false);
							$scope.openAlert("success","","Deleted Successfully.");
						}
					});
				}//end for
			}//end confirm
		}
	};

	//Check exist group
  	function isExistGroup(group){
  		for(var i=0 ; i<$scope.user_has_group.length; i++){
  			if($scope.user_has_group[i]._data.path == group){
  				return true;
  			}
  		}
  		return false;
  	};
	
	//---------- END GROUP TAB -----------
	//----------ROLES TAB---------
	// "Add role to user" pop-up appears
	$scope.openRole = function(){
		$scope.closeAlert();
		$scope.popupRole = true;
	};

	// "Add role to user" pop-up disappears
	$scope.closeRole = function(){
		$scope.popupRole = false;
	};
	//Remove roles of group
	$scope.removeRole = function(){
		//get all checked checkbox
		var arrRole = [];
		$(".chkRole_add_user:checked").each(function(){
		    arrRole.push( $(this).val());
		});
		if(arrRole.length<=0){
			$('input.checkallRole').prop('checked', false);
			$scope.openAlert("error","","Please select at least a row to delete.");
		}
		else
			if (confirm('Are you sure you want to delete?')){
				
				for(var i = 0; i< arrRole.length; i++){
					var endpoint = "users/" + $scope.uuid + "/roles/" + arrRole[i];
					var options = {
						method:'DELETE',
						endpoint:endpoint
					};
					client.request(options, function(err, data){
						if(err)
							$scope.openAlert("error","","Could not delete role.");
						else{
							$scope.init();
							$('input.checkallRole').prop('checked', false);
							$scope.openAlert("success","","Deleted Successfully.");
						}
					});
				}//end for
			}//end confirm
	};
	//Add role to group function
	$scope.addRole = function(){
		//get all checked checkbox
		var arrRole_add_user = [];
		$(".chkRole_add_user:checked").each(function(){
		    arrRole_add_user.push( $(this).val());
		});
		for(var i =0; i< arrRole_add_user.length; i++){
			var options ={
				method : "POST",
				endpoint : "users/" + $scope.uuid + "/roles/" + arrRole_add_user[i]
			};
			client.request(options, function(err, data){
				if(err)
					$scope.openAlert("error","","Could not add role.");
				else{
					$scope.popupRole = false;
					$scope.init();
					$scope.openAlert("success","","Created Successfully.");
				}
			});
		}
	};
	//Check exist role
  	function isExistRole(roleName){
  		for(var i=0 ; i<$scope.role_of_user.length; i++){
  			if($scope.role_of_user[i]._data.roleName == roleName){
  				return true;
  			}
  		}
  		return false;
  	};	
	//----------END ROLES TAB---------
};
//=========END USER DETAIL CONTROLLER=========