//==========GROUP CONTROLLER==========
function GroupController($scope){
	document.title = 'Group Management';
	//Check, Uncheck all checkbox
	$scope.check = false;
  	$scope.checkAll = function(){
  		alert($scope.check);
  		if($scope.check)
  			$('input.chk').prop('checked', true);
  		else
  			$('input.chk').prop('checked', false);
  	}
  	$scope.checkItem = function(){
  		var ok=false;
  		$("input.chk:not(:checked)").each(function() {ok=true;});
        if(ok){        	
       		$('input.checkAll').prop('checked', false);
       		$scope.check = false;
        }
        else{
        	$('input.checkAll').prop('checked', true);
        	$scope.check = true;
        }
  	}//End check, uncheck checkbox
	$scope.path   = "";
	$scope.title  = "";
	$scope.groups = [];

	//Regular Expression
	$scope.titleRegex = /^\w*$/;
	$scope.pathRegex = /^\w*$/;
	
	//Pagination
	// $scope.pages =[];
	// $scope.currentPage = 0;
	// $scope.pageSize = 10;
	// $scope.numberOfPages=0;
	// $scope.totalRecord=0;
	// $scope.paging = function(index){
 //  		$scope.currentPage=Math.ceil(index);
 //  	}

	// //Get all group
	// var options = {
	//     type: "groups",
	//     qs:{limit: 999,ql:"order by path"}
	// };
	// $scope.init = function(){
	// 	$scope.groups = [];
	// 	$scope.pages=[];
	// 	client.createCollection(options, function(err, data){
	// 		if(err)
	// 			console.log("Could not get groups");
	// 		else{
	// 			$scope.groups= data._list;

	// 			$scope.$apply($scope.groups);
	// 			console.log($scope.groups);
	// 			$scope.totalRecord = $scope.groups.length;
	// 			$scope.numberOfPages=Math.ceil($scope.groups.length/$scope.pageSize);                
	// 			// $scope.$apply($scope.numberOfPages);
	// 			$scope.$apply($scope);

	// 			for(var i =1 ; i<=$scope.numberOfPages;i++){
	// 				$scope.pages.push(i);
	// 			}		
	// 			$scope.$apply($scope.pages);
	// 			$('#myModal').hide();
	// 		};//End else
	// 	});
	// };
	//Get all group
    var options={
        type: "groups",
        qs:{ql:"order by path"}
    };
    $scope.init = function(){
    	$('#myModal').show();
        $scope.groups = [];
        var data;
        client.createCollection(options, function(err, data){
            if(err){            	
                $('#myModal').hide();
                $scope.openAlert("error","", "Could not get group.");
            }else{
                $scope.drawGroup = function(){
                    data.fetch(function(err, d){
                        $('#prevPage').hide();
                        $('#nextPage').hide();

                        //reset the pointer
                        data.resetEntityPointer();
                        $scope.groups= data._list;
                        $scope.$apply($scope.groups);                        

                        //if(data.hasNextPage() && data._previous.lenght!=0)
                        if(data.hasNextPage())
                        	$('#nextPage').show();

                        if(data.hasPreviousPage())
                            $('#prevPage').show();

                        $('#myModal').hide();
                    });
                } //End drawGroup function

                $scope.prevPage = function(){
                    data.getPreviousPage(function(err, data){
                        if(err)
                            $scope.openAlert("error","","Could not get previous page.");
                        else
                            $scope.drawGroup();
                    });
                }
                $scope.nextPage = function(){
                    data.getNextPage(function(err, data){
                        if(err)
                            $scope.openAlert("error","","Could not get next page.");
                        else
                            $scope.drawGroup();
                    });
                };
                $scope.drawGroup();
            };//End else
        });
    };

	//Initializing a pop-up
	$scope.opts = {
	    backdropFade: true,
	    dialogFade:true
  	};
	// "Create new group" pop-up appears
	$scope.open = function(){
		$scope.path="";
		$scope.title="";
		$scope.closeAlert();
		$scope.shouldBeOpen = true;
	}

	// "Create new group" pop-up disappears
	$scope.close = function(){
		$scope.shouldBeOpen = false;
	}
	//DELETE GROUP
	$scope.deleteGroup = function(){
		var array = [];
		//get all checked checkbox
		$(".chk:checked").each(function(){
		    array.push( $(this).val());
		});
		if(array.length<=0)
			$scope.openAlert("error","","Please select at least a row to delete.");
		else{
			if (confirm('Are you sure you want to delete?')){				
				for(var i =0; i< array.length; i++){
					var endpoint='groups/' + array[i] ;
					var options={
						method:'DELETE',
						endpoint:endpoint
					};
					client.request(options, function(err, data){
						if(err)
							$scope.openAlert("error","","Could not delete group.");
						else{
							$scope.openAlert("success","","Deleted Successfully.");
							$scope.init();
						}
					});
				}//End for
			}//End confirm
		}
	};

	//Create group function
	$scope.createGroup = function(){		
		var options ={
			method : "POST",
			endpoint: "groups",
			body: {
				path : $scope.path,
				title : $scope.title
			}
		};
		$scope.shouldBeOpen = false;
		client.request(options, function(err, data){
			if(err)
				$scope.openAlert("error","","Could not create a group.");
			else{
				$scope.init();
				$scope.openAlert("success","","Created Successfully.");
			}
		});
	}

	//Navigate to Group detail page
	$scope.navDetail = function(group_uuid){
		location.href = "#/admin/groups/" +  group_uuid;
	};

	//Sort
	$scope.predicate = 'path';
  	$('.sort').click(function(){
  		if($scope.reverse){
  			$('#sortImg').remove();
  			$(this).append('<span id="sortImg"> &#9650;</span>');
  		}
  		else{
  			$('#sortImg').remove();
  			$(this).append('<span id="sortImg"> &#9660;</span>');
  		}
  	});
  	//Filter
    $scope.search='';
	$scope.filterByPath = function(data){
		return data._data.path.toLowerCase().search($scope.search.toLowerCase()) != -1;
	}
    
	//ALERT
	$scope.openAlert = function(type, title, message){
  		$('#alertMsg').html('<div class="alert alert-'+type+'"><strong>'+title+'</strong> '+message+'</div>').show('slow');  		
  		setTimeout(function(){$('#alertMsg').hide('slow')},4000);
  	};
  	$scope.closeAlert = function(){
  		$('#alertMsg').hide();
  	};
};
//==========END GROUP CONTROLLER==========

//====================GROUP DETAIL CONTROLLER====================
function GroupDetailController($scope, $routeParams){

	$scope.abc = function(){
		alert("asd");
	}
	$scope.check = true;
  	$scope.checkAll = function(data){
  		console.log(data)
  		if(data)
  		$('input.chkUsers').prop('checked', true);
  		else
  		$('input.chkUsers').prop('checked', false);
  	}
  	$scope.check1 = function(){
  		var d=false;
  		$("input.chkUsers:not(:checked)").each(function() {
                  d=true;
        });
        if(d){
       		 $('input.checkAll').prop('checked', false);
       		 $scope.check = true;
        }
        else{
        	$('input.checkAll').prop('checked', true);
        	$scope.check = false;
        }
  	}

	document.title = 'Detail Group';
	$scope.uuid = $routeParams.groupid;
	$scope.group ={};

	$scope.users_add_group =[];
	$scope.users_in_group =[];

	$scope.role_add_group =[];
	$scope.role_of_group =[];

	//Regular Expression
	//$scope.titleRegex = /^\w*$/;
	$scope.pathRegex = /^\w*$/;
	$scope.titleRegex = /^([0-9a-zA-Z ])*$/;

	//ALERT
	$scope.openAlert = function(type, title, message){
  		$('#alertMsg').html('<div class="alert alert-'+type+'"><strong>'+title+'</strong> '+message+'</div>').show('slow');  		
  		setTimeout(function(){$('#alertMsg').hide('slow')},3000);
  	};
  	$scope.closeAlert = function(){
  		$('#alertMsg').hide();
  	};
  	$scope.init1 = function(){
		var options={
			method: "GET",
			endpoint: "groups/" + $scope.uuid
		}

		client.request(options, function(err, data){
			if(err){
				console.log("Could not get group");}
			else{
				$scope.group=data.entities[0];
				$scope.$apply($scope.group);
			}
		});
	}
	$scope.init2 = function(){
		//List user in group
		var options = {type: "groups/" + $scope.uuid + "/users"};
		client.createCollection(options, function(err, data){
			if(err){
				$scope.openAlert("error","Error!","Could not get data");}
			else{
				//console.log(data);
				$scope.users_in_group = data._list;
				$scope.$apply($scope.users_in_group);
				//Load list user add to group
			  	var options = {
					type : "users"
				};

				client.createCollection(options, function(err, data){
					if(err){
						console.log("Could not get users");}
					else{
						while(data.hasNextEntity()){
							var obj=data.getNextEntity();
							if(!isExistUser(obj.get("username"))){
								$scope.users_add_group.push(obj.get("username"));
							}
						}
					}
				});//end request
			}//end else
		});//end get user in group 
	}
	$scope.init3 = function(){

		//List role of group
		var options = {type: "groups/" + $scope.uuid + "/roles"};
		client.createCollection(options, function(err, data){
			if(err){
				console.log("Could not get role of group");
			}
			else{
				$scope.role_of_group = data._list;
				$scope.$apply($scope.role_of_group);

				var options = { type : "roles" };

				client.createCollection(options, function(err, data){
					if(err){
						console.log("Could not get roles");}
					else{
						$scope.role_add_group = [];
						while(data.hasNextEntity()){
							var obj=data.getNextEntity();
							if(!isExistRole(obj.get("roleName"))){
								$scope.role_add_group.push(obj.get("roleName"));}
						}
					}
				});//end request
			}//end else
		}); //end get list role of group
	}; //End init function

	//---------- DETAIL TAB -----------
	//Navigate to Groups page
	$scope.cancel = function(){
		$scope.closeAlert;
		location.href = "#/admin";
	};

	//Update Group
	$scope.update = function(uuid){
		var options={
			method: 'PUT',
			endpoint: 'groups/'+ uuid,
			body:{
				path 	 : $scope.group.path,
				title 	 : $scope.group.title
			}
		};
		client.request(options, function(err,data){
			if(err){
				$scope.openAlert("success","","Could not update.");}
			else{
				$scope.init();
				$scope.openAlert("success","","Updated Successfully.");
			}
		});
	};
	//---------- END DETAIL TAB -----------
	//---------- USER TAB -----------
	//Initializing "Add user to group" pop-up
	$scope.opts = {
	    backdropFade: true,
	    dialogFade:true
  	};

	// "Add user to group" pop-up appears
	$scope.openUser = function(){
		$scope.closeAlert();
		$scope.popupUser = true;
	};

	// "Add new user to group" pop-up disappears
	$scope.closeUser = function(){		
		$scope.popupUser = false;
	};

	//Add user to group function
	$scope.addUser = function(){
		//get all checked checkbox
		var arrUser_add_group = [];
		$(".chkUser_add_group:checked").each(function(){
		    arrUser_add_group.push( $(this).val());
		});

		for(var i =0; i< arrUser_add_group.length; i++){
			var options ={
				method : "POST",
				endpoint : "groups/" + $scope.uuid + "/users/" + arrUser_add_group[i]
			};
			
			client.request(options, function(err, data){
				if(err){
					$scope.openAlert("error","","Could not add user.");}
				else{					
					$scope.popupUser = false;
					$scope.init2();
					$scope.openAlert("success","","Created Successfully.");
				}
			});
		}//end for
	};

	//Remove user in group
	$scope.removeUser = function(){
		//get all checked checkbox
		var arrUsers = [];
		$(".chkUsers:checked").each(function(){
		    arrUsers.push( $(this).val());
		});
		if(arrUsers.length<=0){
			$('input.checkAllUser').prop('checked', false);
			$scope.openAlert("error","","Please select at least a row to delete.");
		}
		else{
			if (confirm('Are you sure you want to delete?')){			
				for(var i = 0; i< arrUsers.length; i++){
					var endpoint = "groups/" + $scope.uuid + "/users/" + arrUsers[i];
					var options = {
						method:'DELETE',
						endpoint:endpoint
					};
					client.request(options, function(err, data){
						if(err){
							$scope.openAlert("error","","Could not delete user.");}
						else{
							$scope.init2();
							$('input.checkAllUser').prop('checked', false);
							$scope.openAlert("success","","Deleted Successfully.");
						}
					});
				}//end for
			}//end confirm
		}
	}
	
	//Check exist user
  	function isExistUser(username){
  		for(var i=0 ; i<$scope.users_in_group.length; i++){
  			if($scope.users_in_group[i]._data.username == username){
  				return true;
  			}
  		}
  		return false;
  	};
	//---------- END USER TAB -----------

	//---------- ROLE TAB -----------
	// "Add role to group" pop-up appears
	$scope.openRole = function(){
		$scope.closeAlert();
		$scope.popupRole = true;
	};

	// "Add new user to group" pop-up disappears
	$scope.closeRole = function(){
		$scope.popupRole = false;
	};
	//Add role to group function
	$scope.addRole = function(){
		//get all checked checkbox
		var arrRole_add_group = [];
		$(".chkRole_add_group:checked").each(function(){
		    arrRole_add_group.push( $(this).val());
		});
		for(var i =0; i< arrRole_add_group.length; i++){
			var options ={
				method : "POST",
				endpoint : "groups/" + $scope.uuid + "/roles/" + arrRole_add_group[i]
			};			
			client.request(options, function(err, data){
				if(err){
					$scope.openAlert("error","","Could not add role.");}
				else{					
					$scope.popupRole = false;
					$scope.init3();
					$scope.openAlert("success","","Created Successfully.");
				}
			});
		}//end for
	};
	//Check exist role
  	function isExistRole(roleName){
  		for(var i=0 ; i<$scope.role_of_group.length; i++){
  			if($scope.role_of_group[i]._data.roleName == roleName){
  				return true;
  			}
  		}
  		return false;
  	};	

	//Remove roles of group
	$scope.removeRole = function(){
		//get all checked checkbox
		var arrRoles = [];
		$(".chkRoles:checked").each(function(){
			arrRoles.push( $(this).val());
		});
		if(arrRoles.length<=0){
			$('input.checkallRole').prop('checked', false);
			$scope.openAlert("error","","Please select at least a row to delete.");
		}
		else{
			if (confirm('Are you sure you want to delete?')){
				
				for(var i = 0; i< arrRoles.length; i++){
					var endpoint = "groups/" + $scope.uuid + "/roles/" + arrRoles[i];
					var options = {
						method:'DELETE',
						endpoint:endpoint
					};
					client.request(options, function(err, data){
						if(err)
							$scope.openAlert("error","","Could not delete role.");
						else{							
							$scope.init3();
							$('input.checkallRole').prop('checked', false);							
							$scope.openAlert("success","","Deleted Successfully.");
						}
					});
				}//end for
			}//end confirm
		}
	};
	//---------- END ROLE TAB -----------
	
}
//==========END GROUP DETAIL CONTROLLER==========