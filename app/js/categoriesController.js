//USER CONTROLLER
function categoriesController($scope) {
	$scope.word = /^([0-9a-zA-Z ])*$/;
	document.title = 'Category';
	$scope.titleRegex = /^([0-9a-zA-Z.!-?/])+$/;
	//Initializing information about User
	$scope.title   = "";
	$scope.name       = "";
	$scope.search = '';
  	$scope.filterByTitle = function(data){
		return data._data.title.toLowerCase().search($scope.search.toLowerCase()) != -1;
	}

	//ALERT
	$scope.openAlert = function(type, title, message){
  		$('#alertMsg').html('<div class="alert alert-'+type+'"><strong>'+title+'</strong> '+message+'</div>').show('slow');  		
  		setTimeout(function(){$('#alertMsg').hide('slow')},4000);
  	};
  	
	//Get all user
	var options={
		type: "categories",
	}
	//Initializing a pop-up

	$scope.init = function(){
		$('#myModal').show();
        $scope.categories = [];
        var data;
        client.createCollection(options, function(err, data){
            if(err){
               //showMess('#messLisImg', '<div class="alert alert-error"><b>Error</b> Could not get list Stores!</div>');
                $('#myModal').hide();
            }else{
            	console.log(data);
                $scope.drawPages2 = function(){
                    data.fetch(function(err, d){
                        $('#prevPage').hide();
                        $('#nextPage').hide();
                        //reset the pointer
                        data.resetEntityPointer();
                        $scope.categories= data._list;
                        $scope.categories.sort();
                        $scope.$apply($scope.categories);
                        if(data.hasNextPage())
                          $('#nextPage').show();
                        if(data.hasPreviousPage())
                            $('#prevPage').show();

                        $('#myModal').hide();
                    });
                } //End drawUser function

                $scope.prevPage = function(){
                    data.getPreviousPage(function(err, data){
                        if(err)
                            alert("err");
                        else
                            $scope.drawPages2();
                    });
                }
                $scope.nextPage = function(){
                    data.getNextPage(function(err, data){
                        if(err)
                            alert("err");
                        else
                            $scope.drawPages2();
                    });
                };
                $scope.drawPages2();
            };//End else
        });
    };

	$scope.opts = {
	    backdropFade: true,
	    dialogFade:true
  	};

  	//"Create new user" pop-up appears
	$scope.open = function(){
		$scope.shouldBeOpen = true;
		$scope.title   = "";
	};

	$scope.openDetail = function(data){
		$scope.shouldBeOpenDetail = true;
		$scope.title = data.title;
		$scope.name = data.name;
	}
	//"Create new user" pop-up disappears
	$scope.close = function(){
		$scope.shouldBeOpen = false;
		$scope.shouldBeOpenDetail = false;
	}
	//Delete users function
	$scope.array = [];
	$scope.delete = function(){
		$(".chk:checked").each(function(){
			  $scope.array.push($(this).val());
		});
		if($scope.array.length == 0){
    	   $scope.openAlert("error","","Please select at least a row to delete.");
    	}else
    	if (confirm('Are you sure you want to delete?')){
			//get all checked checkbox
			$('#myModal').show();
			for(var i =0; i< $scope.array.length; i++){
				var endpoint='categories/' + $scope.array[i] ;
				var options={
					method:'DELETE',
					endpoint:endpoint
				};
				client.request(options, function(err, data){
					if(err)
						$scope.openAlert("error","","Could not delete categories.");
					else{
						$('#myModal').hide();
						$scope.init();
						$scope.openAlert("success","","Deleted Successfully.");
					}
				});
			}

		}
	};

	//Create new user function
	$scope.createCategory = function(){
		$('#myModal').show();
		$scope.name = $scope.title.replace(/\s/g, "");
		var options ={
			method : "POST",
			endpoint: "categories",
			body: {
				title : $scope.title,
				name : $scope.name,
			}
		};
		$scope.shouldBeOpen = false;
		client.request(options, function(err, data){
			if(err){
				$('#myModal').hide();
				$scope.openAlert("error","","Catagory exist.");
			}
			else{
				$('#myModal').hide();
				$scope.init();
				$scope.openAlert("success","","Created Successfully.");
				//location.reload();
			}
		});
	};
	$scope.updateCategory = function(){
		$('#myModal').show();
		var options ={
			method : "PUT",
			endpoint: "categories/" + $scope.name,
			body: {
				title : $scope.title
			}
		};
		$scope.shouldBeOpenDetail = false;
		client.request(options, function(err, data){
			if(err){
				$('#myModal').hide();
				$scope.openAlert("error","","Catagory exist.");
			}
			else{
				$('#myModal').hide();
				$scope.init();
				$scope.openAlert("success","","Updated Successfully.");
				//location.reload();
			}
		});
	};

  	//sort
  	$scope.predicate = '_data.title';
  	$('.sort').click(function(){
  		if($scope.reverse){
  			$('#sortImg').remove();
  			$(this).append('<span id="sortImg"> &#9650;</span>');}
  		else{
  			$('#sortImg').remove();
  			$(this).append('<span id="sortImg"> &#9660;</span>');}
  		});

  	$scope.check = true;
  	$scope.checkAll = function(data){
  		console.log(data)
  		if(data)
  		$('input.chk').prop('checked', true);
  		else
  		$('input.chk').prop('checked', false);
  	}
  	$scope.check1 = function(){
  		var d=false;
  		$("input.chk:not(:checked)").each(function() {
                  d=true;
        });
        if(d){
       		 $('input.checkAll').prop('checked', false);
       		 $scope.check = true;
        }
        else{
        	$('input.checkAll').prop('checked', true);
        	$scope.check = false;
        }
  	}
};
