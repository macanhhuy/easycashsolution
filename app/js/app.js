'use strict';

/* App Module */

var app = angular.module('ecs', ['ecsFilters','ngCookies','ui.bootstrap']).
  config(['$routeProvider', function($routeProvider) {
  	
  	$routeProvider.  		
  		when('/login', {templateUrl: 'partials/login/login.html',   controller: LoginController}).
      when('/register', {templateUrl: 'partials/login/register.html',   controller: LoginController}).
  		when('/books', {templateUrl: 'partials/book.html',   controller: BookController}).
  		when('/test', {templateUrl: 'partials/test.html',   controller: TestController}).
     	when('/service', {templateUrl: 'partials/testService.html',   controller: HelloCtrl}).
      when('/admin', {templateUrl: 'partials/admin/admin.html',   controller: AdminController}). 
      when('/admin_home', {templateUrl: 'partials/admin/admin_home.html'}).
      when('/admin/users/:userid', {templateUrl: 'partials/users/user-detail.html',   controller: UserDetailController}).
      when('/admin/groups/:groupid', {templateUrl: 'partials/groups/group-detail.html',   controller: GroupDetailController}).
      when('/users', {templateUrl: 'partials/users/users.html',   controller: UserController}).
      when('/admin/createpage', {templateUrl: 'partials/pages/create.html',   controller: PageCreateController}).
      when('/admin/blogs/:blogsid', {templateUrl: 'partials/blogs/updateblogs.html',   controller: UpdateBlogsController}).
      when('/admin/createblogs', {templateUrl: 'partials/blogs/createblog.html',   controller: CreateBlogsController}).
      when('/admin/pages/:pageid', {templateUrl: 'partials/pages/pageDetail.html',   controller: PageDetailController}).
      when('/admin/createmenu', {templateUrl: 'partials/menus/create.html',   controller: CreateMenuController}).
      when('/admin/menus/:menuid', {templateUrl: 'partials/menus/menuDetail.html',   controller: MenuDetailController}).
      when('/admin/galleries/:galleryid', {templateUrl: 'partials/galleries/galleryDetail.html',   controller: ImageDetailController}).
      when('/admin/stores/:storeid', {templateUrl: 'partials/stores/storeDetail.html',   controller: StoreDetailController}).
      otherwise({redirectTo: '/login'});
    // $locationProvider.html5Mode  (true);
}]);
